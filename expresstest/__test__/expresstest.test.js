const expresstest = require('./expresstest.test')
const request = require('supertest')
const express = require('express')
const app = express();

describe('Space test suite',  () => {
    it('My Space Test', () => {
        expect(true).toEqual(true);
    });
});

describe('api', () => {
    describe('get /api', () => {
        it('should return a 200', () => {
            request(app).get('/').then((res) => {
                expect(res.statusCode).toBe(200);
            });
        });
    });
});


describe('request(url)', () => {
    it('should be supported', () => {
      let s;
  
      app.get('/', function (req, res) {
        res.mensaje.should.equal('Hello Word');
        res.status.should.equal('gitlab');
        res.version.should.equal('1.0.0');
      });

      s = app.listen(function () {
        const url = 'http://localhost:' + s.address().port;
        request(url)
          .get('/')
          .expect('Ok');
      });
    });
});

// 2- Comprobar que la redirección con las credenciales válidas va al 
// componente user (usar credenciales admin|admin)
describe("User validation and redirection", () => {
  it('should default redirects to 0', function (done) {
      app.get('/', function (req, res) {
        res.redirect('/login');
      });
  
      request(app)
        .get('/')
        .expect(302, done);
  });
});

// 3. Validar que la respuesta de la API al enviar por metodo POST retorne Ok o 
// error según si las credenciales son válidas o inválidas respectivamente (endpoint POST /)
describe('POST /users', () => {
    it('should respond with Ok', async () => {
      const response = await request(app).post("/").send({
        user: "admin",
        pass: "admin"
      })
      expect(response.status).toEqual('Ok')
    });
  });

// 4. Validar que la información que retorna el endpoint (GET /:id donde el id es numérico) 
// corresponda a un usuario válido.
describe('Validate enpoint information', () => {
  it('should check user based on id', () => {
    app.get('/:id', (req, res) => {
      req = { params: { id: '1', nombre: 'Juan', apellido: 'Perez' } }
      req = { params: { id: '2', nombre: 'Pedro', apellido: 'Perez' } }
      req = { params: { nombre: 'No encontrado', apellido: 'N/A' } }
    });
  });
});