module.exports = {
    moduleFileExtensions: ['js', 'vue'],
    transformIgnorePatterns: ['<rootDir>/node_modules/'],
    resolver: 'jest-node-exports-resolver'
}